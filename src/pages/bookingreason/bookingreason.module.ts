import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookingreasonPage } from './bookingreason';

@NgModule({
  declarations: [
    BookingreasonPage,
  ],
  imports: [
    IonicPageModule.forChild(BookingreasonPage),
  ],
})
export class BookingreasonPageModule {}
