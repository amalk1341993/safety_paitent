import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-bookingreason',
  templateUrl: 'bookingreason.html',
})
export class BookingreasonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingreasonPage');
  }


       goBack(){
    this.navCtrl.pop();
  }

   open_page(page){
  this.navCtrl.push(page);
  }

  success(){
     let modal = this.modalCtrl.create('SuccessPage');
     modal.present();
   }

}
