import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArticlecategoryPage } from './articlecategory';

@NgModule({
  declarations: [
    ArticlecategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ArticlecategoryPage),
  ],
})
export class ArticlecategoryPageModule {}
