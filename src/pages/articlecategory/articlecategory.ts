import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-articlecategory',
  templateUrl: 'articlecategory.html',
})
export class ArticlecategoryPage {

	   shownGroup = null;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


    toggleGroup(group) {
      if (this.isGroupShown(group)) {
          this.shownGroup = null;
      } else {
          this.shownGroup = group;
      }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticlecategoryPage');
  }

     goBack(){
    this.navCtrl.pop();
  }

   open_page(page){
  this.navCtrl.push(page);
  }

}
