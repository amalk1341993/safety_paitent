import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-searchresult',
  templateUrl: 'searchresult.html',
})
export class SearchresultPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchresultPage');
  }


  /* COMMON-NAVIGATION-CONTROL-FUNCTION */

  open_page(page){
  this.navCtrl.push(page);
  }

   goBack(){
    this.navCtrl.pop();
  }

  map(){
     let modal = this.modalCtrl.create('MapPage');
     modal.present();
   }

   sortfilter(){
     let modal = this.modalCtrl.create('FiltersortPage');
     modal.present();
   }

  /*------------------*/

}
