import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,Content,Slides,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-contacthistory',
  templateUrl: 'contacthistory.html',
})
export class ContacthistoryPage {

  currentIndex:any;
  slidertab:any;

    @ViewChild(Content) content: Content;
  @ViewChild('mySlider') slider: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public viewCtrl: ViewController) {
    let id = this.navParams.get("id");
  this.slidertab = id;
  console.log("id", id);
  setTimeout(() => {
  this.goToSlide(id);
  }, 500)
  this.slidertab = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiltersortPage');
  }

  goToSlide(id) {
this.slider.slideTo(id, 500);
}

slideChanged() {
let currentIndex = this.slider.getActiveIndex();
this.slidertab = currentIndex;
console.log("Current index is", currentIndex);
}

      goBack(){
    this.navCtrl.pop();
  }

  open_page(page){
  this.navCtrl.push(page);
  }

  chat(){
     let modal = this.modalCtrl.create('ChatPage');
     modal.present();
   }

     videocall(){
     let modal = this.modalCtrl.create('VideocallPage');
     modal.present();
   }

      audiocall(){
     let modal = this.modalCtrl.create('AudiocallPage');
     modal.present();
   }

}
