import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,MenuController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
    // this.menuCtrl.enable(false);
  }

  otp(){
     let modal = this.modalCtrl.create('OtpPage');
     modal.present();
   }

     goBack(){
    this.navCtrl.pop();
  }

}
