import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController} from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-askques',
  templateUrl: 'askques.html',
})
export class AskquesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AskquesPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }


}
