import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AskquesPage } from './askques';

@NgModule({
  declarations: [
    AskquesPage,
  ],
  imports: [
    IonicPageModule.forChild(AskquesPage),
  ],
})
export class AskquesPageModule {}
