import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-articledetail',
  templateUrl: 'articledetail.html',
})
export class ArticledetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticledetailPage');
  }

     goBack(){
    this.navCtrl.pop();
  }

}
