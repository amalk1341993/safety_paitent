import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-articlelist',
  templateUrl: 'articlelist.html',
})
export class ArticlelistPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticlelistPage');
  }


     goBack(){
    this.navCtrl.pop();
  }

   open_page(page){
  this.navCtrl.push(page);
  }

}
