import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,MenuController  } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

 shownGroup = null;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
    // this.menuCtrl.enable(true);
  }

  /* COMMON-NAVIGATION-CONTROL-FUNCTION */

  open_page(page){
  this.navCtrl.push(page);
  }


  forgot(){
     let modal = this.modalCtrl.create('ForgotPage');
     modal.present();
   }

  /*------------------*/

}
