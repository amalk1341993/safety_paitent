import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentdetailPage } from './appointmentdetail';

@NgModule({
  declarations: [
    AppointmentdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentdetailPage),
  ],
})
export class AppointmentdetailPageModule {}
