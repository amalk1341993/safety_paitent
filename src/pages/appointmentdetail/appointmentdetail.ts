import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-appointmentdetail',
  templateUrl: 'appointmentdetail.html',
})
export class AppointmentdetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentdetailPage');
  }

       goBack(){
    this.navCtrl.pop();
  }

  
  open_page(page){
  this.navCtrl.push(page);
  }

}
