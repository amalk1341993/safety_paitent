import { Component,ViewChild,ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { Content } from 'ionic-angular';

declare var google;

@IonicPage()
@Component({
  selector: 'page-resultdetail',
  templateUrl: 'resultdetail.html',
})
export class ResultdetailPage {

	@ViewChild('map') mapElement: ElementRef;
  @ViewChild('videoPlayer') videoplayer: any;
   @ViewChild(Content) content: Content;
   scrollPosition: number = 0;

      map: any;
      tab_scroll:any;
      consultation: string = "visit";

  // id = 'qDuKsiwS5xw';
  // private player;
  // private ytEvent;
  //public version = config.map['ngx-youtube-player'].split('@')[1];
    shownGroup = null;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {

  }

 ionViewDidLoad(){
   this.loadMap();
  }
 
  ngAfterViewInit() {

     this.content.ionScroll.subscribe(($event) => {
       this.scrollPosition = 0;
       console.log(this.scrollPosition);
     });
  }

 loadMap(){
 
    let latLng = new google.maps.LatLng(-34.9290, 138.6010);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  }
    goBack(){
    this.navCtrl.pop();
  }
 // onStateChange(event) {
 //    this.ytEvent = event.data;
 //  }
 //  savePlayer(player) {
 //    this.player = player;
 //  }


  public scrollElement(id) {
  this.tab_scroll = id
    let element = document.getElementById(id);
    this.content.scrollTo(0, element.offsetTop - 70, 500);
  }

  toggleGroup(group) {
      if (this.isGroupShown(group)) {
          this.shownGroup = null;
      } else {
          this.shownGroup = group;
      }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };
 toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
}

ask(){
     let modal = this.modalCtrl.create('AskquesPage');
     modal.present();
   }

   open_page(page){
  this.navCtrl.push(page);
  }


}
