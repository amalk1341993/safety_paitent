import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultdetailPage } from './resultdetail';
// import { YoutubePlayerModule } from 'ng2-youtube-player';

@NgModule({
  declarations: [
    ResultdetailPage,
  ],
  imports: [
  // YoutubePlayerModule ,
    IonicPageModule.forChild(ResultdetailPage),
  ],
})
export class ResultdetailPageModule {}
