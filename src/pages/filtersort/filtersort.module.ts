import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiltersortPage } from './filtersort';

@NgModule({
  declarations: [
    FiltersortPage,
  ],
  imports: [
    IonicPageModule.forChild(FiltersortPage),
  ],
})
export class FiltersortPageModule {}
