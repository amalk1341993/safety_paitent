import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,PopoverController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }

   editpopover(myEvent) {
    let popover = this.popoverCtrl.create('PopoverPage');
    popover.present({
      ev: myEvent
    });
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }


}
