import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { CustomtabComponent } from '../../components/customtab/customtab';
import { FabComponent } from '../../components/fab/fab';

@NgModule({
  declarations: [
    HomePage,
    CustomtabComponent,
    FabComponent
  ],
  imports: [
    IonicPageModule.forChild(HomePage),

  ],
})
export class HomePageModule {}
