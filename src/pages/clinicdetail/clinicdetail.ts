import { Component,ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';

declare var google;

@IonicPage()
@Component({
  selector: 'page-clinicdetail',
  templateUrl: 'clinicdetail.html',
})
export class ClinicdetailPage {

	@ViewChild('map') mapElement: ElementRef;
  @ViewChild('videoPlayer') videoplayer: any;

      map: any
  // id = 'qDuKsiwS5xw';
  // private player;
  // private ytEvent;
  //public version = config.map['ngx-youtube-player'].split('@')[1];
    shownGroup = null;

    constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

 ionViewDidLoad(){
   this.loadMap();
  }

  
 loadMap(){
 
    let latLng = new google.maps.LatLng(-34.9290, 138.6010);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  }
    goBack(){
    this.navCtrl.pop();
  }
 // onStateChange(event) {
 //    this.ytEvent = event.data;
 //  }
 //  savePlayer(player) {
 //    this.player = player;
 //  }


  toggleGroup(group) {
      if (this.isGroupShown(group)) {
          this.shownGroup = null;
      } else {
          this.shownGroup = group;
      }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };
 toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
}

ask(){
     let modal = this.modalCtrl.create('AskquesPage');
     modal.present();
   }

   open_page(page){
  this.navCtrl.push(page);
  }

 
}
