import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClinicdetailPage } from './clinicdetail';

@NgModule({
  declarations: [
    ClinicdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ClinicdetailPage),
  ],
})
export class ClinicdetailPageModule {}
