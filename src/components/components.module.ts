import { NgModule } from '@angular/core';
import { CustomtabComponent } from './customtab/customtab';
import { FabComponent } from './fab/fab';
@NgModule({
	declarations: [CustomtabComponent,
    FabComponent],
	imports: [],
	exports: [CustomtabComponent,
    FabComponent]
})
export class ComponentsModule {}
