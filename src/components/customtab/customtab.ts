import { Component } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';

@Component({
  selector: 'customtab',
  templateUrl: 'customtab.html'
})
export class CustomtabComponent {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

   open_page(page){
  this.navCtrl.push(page);
  }

  profile(){
     let modal = this.modalCtrl.create('ProfilePage');
     modal.present();
   }


}
