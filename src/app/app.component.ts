import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,ModalController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'SigninPage';

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public modalCtrl: ModalController) {
    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {
    this.statusBar.styleDefault();
    this.splashScreen.hide();
    });
  }

edit(){
     let modal = this.modalCtrl.create('EditprofilePage');
     modal.present();
   }

   notification(){
     let modal = this.modalCtrl.create('NotificationPage');
     modal.present();
   }

  logout() {
this.nav.setRoot('LogoutPage');
}

  signin() {
this.nav.setRoot('SigninPage');
}

  settings() {
this.nav.setRoot('SettingsPage');
}

  wallet() {
this.nav.setRoot('WalletPage');
}


}
